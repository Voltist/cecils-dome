##########
# This is where all the rooms, and the connections between them, go.
##########

from adventurelib import *

# A function that adds sub-cardinal directions to a room
room = Room("")
room.add_direction("southwest", "northeast")
room.add_direction("southeast", "northwest")

# English corridors
for i in "nw ne sw se".split(" "):
    exec(i + '_english = Room("You are in a dark corridor strewn with corpses.")')
    exec(i + '_english.corridor = True')

english_courtyard = Room("""
You are in a courtyard. The sky is black and the only light around you
comes from the windows of classrooms.

There are internal corridors to the east and west, both of which run north-south.
""")

english_courtyard.corridor = True

# And their connections
sw_english.north = nw_english
se_english.north = ne_english

english_courtyard.southwest = sw_english
english_courtyard.southeast = se_english
english_courtyard.northwest = nw_english
english_courtyard.northeast = ne_english

# Ms. Robins' classroom
robins_room = Room("""
You are in a classroom. There is paper and books strewn all about the place,
and a few school bags sit on the floor. Pictures and models of frogs
are randomly scattered around the room. There is no one else here.

There is an a-jar door to the west.
""")

robins_room.west = sw_english

# Mr. Spronkens classroom
spronken_room = Room("""
You are in a classroom. There are many posters on the walls, and paper
models of ships lie on the floor. There is no one else here.

There is a door to the east.
""")

spronken_room.east = nw_english

# Ms. Ashby's classroom
ashby_room = Room("""
You are in a classroom. The door is off it's hinges and there are corpses on the
floor. Posters, most featuring symbolism from the film Edward Scissorhands, cover
the back wall. It is bitterly cold here, and there is no one else around.

There is an open doorway to the east.
""")

ashby_room.east = se_english

# Library
library = Room("""
You are in the school library. Shelves of books cover the back wall, and four
detached bookshelves lie on the floor, toppled by some past event. A single,
flickring light illuminates the very large room, and a shiny black goo covers
the floor.

There is pair of double-doors to the east.
""")

library.east = ne_english

# Science corridors
for i in "sw se n".split(" "):
    exec(i + '_science = Room("You are in a dark corridor strewn with corpses.")')
    exec(i + '_science.corridor = True')

science_courtyard = Room("""
You are in a courtyard. The sky is pitch black, but you can discern a couple
of bathtubs just off the path.

There are internal corridors to the east and west.
""")

n_science.southwest = sw_science
n_science.southeast = se_science
science_courtyard.east = se_science
science_courtyard.west = sw_science

# Ms. Caufield's room
caufield_room = Room("""
You are in a classroom. A broken fish tank sits on a table across the room,
and there is a strong smell of rotting flesh. YMCA by the Villiage People can be
distantly heard.

There is a door with a boarded-up window to the north, and a broken double-door
to the south.
""")

caufield_room.north = n_science
caufield_room.south = science_courtyard

# Mr. Pirie's room.
pirie_room = Room("""
You are in a classroom.
There is a large dish-shaped mirror on the far side of the room, and there is
a strong smell of chemicals. A speaker painted with zebra markings draws your
attention.

A door leads eastward.
""")

pirie_room.east = se_science

# Link the english and scinece blocks
canteen_courtyard = Room("""
You are in a courtyard. There is a roof above you, and the school canteen
blocks your view of the far end of the valley. It is very dark here.

A flight of stairs to the east, and two entrances to the southeast and southwest.
""")

canteen_courtyard.southwest = nw_english
canteen_courtyard.southeast = ne_english

science_english_stairs = Room("""
You are at the top of a flight of outdoor stairs. The sky is still black. There
is a very large rock crushing the closest corner of a nearby building.

A path leads to a building entrance to the north, and all other entrances
are blocked.
""")

science_english_stairs.west = canteen_courtyard
science_english_stairs.north = sw_science
