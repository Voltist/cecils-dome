##########
# For commiting cookie crime
##########

from adventurelib import *
from random import choice

@when("commit cookie crime")
def cookie():

    place = choice(["your pocket", "the pocket of a nearby corpse",
    "a hovering portal", "your pocket", "your pocket"])

    flavor = choice(["chocolate chip", "salted caramel",
    "cranberry and white chocolate", "ginger"])

    way = choice(["nibble at", "devour", "slowly grind away at",
    "inhale"])

    say(f"""You reach into {place} and pull out a {flavor} flavoured cookie,
    then {way} it.""")
