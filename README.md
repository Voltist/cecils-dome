# Cecils Dome

A text adventure game set at Logan Park High School, NZ.

## Plot Plans (SPOILERS)

* Wake up in Ms. Robins' room.
* Find sword in Mr. Spronken's room (enforced by note in Ms. Ashby's room).
* Use sword to defeat dark minion.
* Learn of Mr. Pirie's efforts to create hero device (begin way to science block).
* Puzzle minigame with Mr. Howard-Smith NPC.
* Collect device but learn of resources needed from tech/art block.
* Various encounters on way to tech/art block.
* Collect some sort of thing, but more things needed from music block.
* Fight with etherial drumsticks.
* To library for boss fight.
