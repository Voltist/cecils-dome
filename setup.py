##########
# This is where various variables are setup.
##########

from adventurelib import *

# Get rooms
import rooms

# Setup start location
location = rooms.robins_room

# Setup inventory
inventory = Bag([])
