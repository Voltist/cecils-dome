##########
# These are all the commands.
##########

from adventurelib import *
from setup import *

# Get cookie crime
from cookies import *

# "Look around the room, see what you can find!"
@when("look")
def look():
    global location
    say(location) # "You are here..."

    # Print possible directions of exit if corridor
    try:
        if location.corridor:
            if len(location.exits()) == 1:
                say("You can go " + location.exits()[-1] + ".")
            elif len(location.exits()) > 1:
                say("You can go " + ", ".join(location.exits()[:-1]) + " or " +
                location.exits()[-1] + ".")
    except AttributeError:
        pass

# For going places
@when("go DIRECTION")
def go(direction):
    global location

    # Check if player can go that way
    if direction in location.exits():
        room = location.exit(direction)
    else:
        say("There is nothing that way.")
        return

    # Update location or not
    if room:
        location = room
        say(f"You go {direction}.")
    else:
        say("There is nothing that way.")
        return

    # Look around
    look()
