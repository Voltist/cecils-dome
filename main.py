from adventurelib import *
import art
import os

# Stuff from other files
import rooms
from setup import *
from commands import *

# Intro
os.system("clear")
print("WELCOME TO")
print(art.text2art("CECIL'S DOME"))

print ("""
    YOU WAKE UP, HEAD POUNDING. LYING ON THE FLOOR, YOU LOOK OUT
    A NEARBY WINDOW TO SEE NOTHING BUT DARKNESS. THERE IS A STRONG
    SMELL OF BURNING WOOD, AND YOU HEAR DISTAND RUMBLING.
""".strip("\n") + "\n")

look()

# Start the game
start()
